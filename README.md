Code for recognizing simple captacha's.

#Dependencies
Sckit
numpy
scipy

This was a hackerank.com challenge. The captchas have the following constraints:

- the number of characters remains the same each time  
- the font and spacing is the same each time  
- the background and forground colors and texture, remain largely the same
- there is no skew in the structure of the characters.  
- the captcha generator, creates strictly 5-character captchas, and each of the characters is either an upper-case character (A-Z) or a numeral (0-9)/.  